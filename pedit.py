import curses
import curses.ascii

class PrettyTextBox:
    
    def __init__(self, win):
        self.win = win
        self.maxy, self.maxx = win.getmaxyx()
        self.maxy = self.maxy - 1
        self.maxx = self.maxx - 1
        win.keypad(1)

    def _end_of_line(self, y):
        last = self.maxx
        while True:
            if curses.ascii.ascii(self.win.inch(y, last)) != curses.ascii.SP:
                last = min(self.maxx, last + 1)
                break
            elif last == 0:
                break
            last = last - 1
        return last

    def do_command(self, ch):
        y, x = self.win.getyx()
        if ch == curses.ascii.SP:
            self._pretty_print()
        elif curses.ascii.isprint(ch):
            if y <= self.maxy and x <= self.maxx:
                try:
                    self.win.addch(ch)
                except curses.error:
                    pass
        elif ch == curses.KEY_LEFT:
            if x > 0:
                self.win.move(y, x - 1)
        elif ch == curses.KEY_RIGHT:
            if x < self.maxx:
                self.win.move(y, x + 1)
        elif ch == curses.KEY_UP:
            if y > 0:
                self.win.move(y - 1, x)
        elif ch == curses.KEY_DOWN:
            if y < self.maxy:
                self.win.move(y + 1, x)
        elif ch == curses.KEY_BACKSPACE:
            if x > 0:
                self.win.move(y, x - 1)
                self.win.delch()
        elif ch == curses.ascii.NL:
            if y < self.maxy:
                self.win.move(y + 1, 0)
        elif ch == curses.ascii.ESC:
            return 0
        return 1

    def gather(self):
        result = ""
        for y in range(self.maxy + 1):
            self.win.move(y, 0)
            stop = self._end_of_line(y)
            if stop == 0:
                continue
            for x in range(self.maxx + 1):
                if x > stop:
                    break
                result = result + chr(curses.ascii.ascii(self.win.inch(y, x)))
        return result

    def edit(self):
        while 1:
            ch = self.win.getch()
            if not ch:
                continue
            if not self.do_command(ch):
                break
            self.win.refresh()

    def _pretty_print(self):
        word = [None] + self.gather().split()
        slack = [[None for x in range(len(word))] for x in range(len(word))]
        table = [0] * len(word)
        back_pointer = [0] * len(word)

        for i in range(1, len(word)):
            slack[i][i] = self.maxx - len(word[i])
            for j in range(i + 1, len(word)):
                slack[i][j] = slack[i][j - 1] - (len(word[j]) + 1)

        for j in range(1, len(word)):
            table[j], back_pointer[j] = min(
                [ (table[i - 1] + slack[i][j]**2, i) for i in range(1, j + 1) if slack[i][j] >= 0 ] 
            )

        opt = []
        
        j = len(word) - 1
        while j > 0:
            opt.insert(0, j)
            j = back_pointer[j] - 1

        self.win.erase()
        self.win.move(0, 0)

        i = 1
        for j in opt[:len(opt) - 1]:
            for k in range(i, j + 1):
                self.win.addstr(word[k] + ' ')
            y, _ = self.win.getyx()
            self.win.move(y + 1, 0)
            i = j + 1

        for k in range(i, opt[len(opt) - 1] + 1):
            self.win.addstr(word[k] + ' ')

def main(stdscr):
    w = curses.newwin(100, 30, 1, 2)
    box = PrettyTextBox(w)
    box.edit()

curses.wrapper(main)
