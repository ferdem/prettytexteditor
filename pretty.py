import sys

input_file = open(sys.argv[1], 'r')
input_as_string = input_file.read()
length = int(sys.argv[2])

word = [None] + input_as_string.split()
slack = [[None for x in range(len(word))] for x in range(len(word))]
table = [0] * len(word)
back_pointer = [0] * len(word)

for i in range(1, len(word)):
    slack[i][i] = length - len(word[i])
    for j in range(i + 1, len(word)):
        slack[i][j] = slack[i][j - 1] - (len(word[j]) + 1)

for j in range(1, len(word)):
    table[j], back_pointer[j] = min(
        [ (table[i - 1] + slack[i][j]**2, i) for i in range(1, j + 1) if slack[i][j] >= 0 ] 
    )

opt = []

j = len(word) - 1
while j > 0:
    opt.insert(0, j)
    j = back_pointer[j] - 1

i = 1
for j in opt:
    for k in range(i, j + 1):
        print(word[k], end = " ")
    print()
    i = j + 1
